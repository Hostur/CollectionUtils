﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;

namespace CollectionExtension
{
  /// <summary>
  /// Random implementation that works for Mono.
  /// </summary>
  public static class Random
  {
    private static System.Random _random = new System.Random();

    public static int Range(int value1, int value2)
    {
      _random = new System.Random(Guid.NewGuid().ToByteArray().First());
      return _random.Next(value1, value2);
    }

    public static float Range(float value1, float value2)
    {
      return ((float)_random.NextDouble() * (value2 - value1)) + value1;
    }

    public static float Value => (float)_random.NextDouble();
  }

  public static class CollectionExtension
  {
    private const byte F_STEP = 16;
    private const byte STEP = 15;

    /// <summary>
    /// Invoke action on each element in given collection.
    /// </summary>
    /// <typeparam name="T">Type of collection elements.</typeparam>
    /// <param name="collection">Ienumerable collection.</param>
    /// <param name="action">Action to invoke.</param>
    public static void Each<T>(this IEnumerable<T> collection, Action<T> action)
    {
      foreach (var item in collection)
      {
        action(item);
      }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Resize<T>(ref T[] collection, int capacity) where T : struct 
    {
      if (collection.Length == capacity)
      {
        return;
      }

      T[] tmp = new T[capacity];

      for (int i = 0; i < capacity; i++)
      {
        if (i < collection.Length)
        {
          tmp[i] = collection[i];
        }
      }

      collection = tmp;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Each<T>(this T[] collection, Action<T> action) where T : struct
    {
      int i;
      for (i = 0; i < (collection.Length & ~STEP); i += F_STEP)
      {
        action(collection[i]);
        action(collection[i + 1]);
        action(collection[i + 2]);
        action(collection[i + 3]);
        action(collection[i + 4]);
        action(collection[i + 5]);
        action(collection[i + 6]);
        action(collection[i + 7]);
        action(collection[i + 8]);
        action(collection[i + 9]);
        action(collection[i + 10]);
        action(collection[i + 11]);
        action(collection[i + 12]);
        action(collection[i + 13]);
        action(collection[i + 14]);
        action(collection[i + 15]);
      }
      for (i = (collection.Length & ~STEP); i < collection.Length; i++)
      {
        action(collection[i]);
      }

    #region SIMD
    // SIMD is much slower than unrolled loop, its not a right time to use it.
    //if (Vector.IsHardwareAccelerated)
    //{
    //    //Console.WriteLine("SIMD");
    //    for (var i = 0; i < collection.Length; i += Vector<T>.Count)
    //    {
    //        Vector<T> tmp = new Vector<T>(collection, i);

    //        for (int k = 0; k < Vector<T>.Count; k++)
    //        {
    //            action(tmp[k]);
    //        }
    //    }
    //}
    //else
    //{
    //      int i;
    //      for (i = 0; i < (collection.Length & ~STEP); i += F_STEP)
    //      {
    //          action(collection[i]);
    //          action(collection[i + 1]);
    //          action(collection[i + 2]);
    //          action(collection[i + 3]);
    //          action(collection[i + 4]);
    //          action(collection[i + 5]);
    //          action(collection[i + 6]);
    //          action(collection[i + 7]);
    //          action(collection[i + 8]);
    //          action(collection[i + 9]);
    //          action(collection[i + 10]);
    //          action(collection[i + 11]);
    //          action(collection[i + 12]);
    //          action(collection[i + 13]);
    //          action(collection[i + 14]);
    //          action(collection[i + 15]);
    //      }
    //      for (i = (collection.Length & ~STEP); i < collection.Length; i++)
    //      {
    //          action(collection[i]);
    //      }
    //  }
    #endregion
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static void Assert<T>(this T[] collection, int index)
    {
      if (collection == null)
      {
        throw new Exception("Cannot remove element from null collection.");
      }
      if (collection.Length < index)
      {
        throw new Exception("Given index exceed collection length.");
      }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static void Assert<T>(this IList<T> collection, int index)
    {
      if (collection == null)
      {
        throw new Exception("Cannot remove element from null collection.");
      }
      if (collection.Count < index)
      {
        throw new Exception("Given index exceed collection length.");
      }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static void Assert<T>(this IEnumerable<T> collection, int index)
    {
      if (collection == null)
      {
        throw new Exception("Collection is null.");
      }
      if (collection.Count() < index)
      {
        throw new Exception("Collection is too short.");
      }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void RemoveAt<T>(ref T[] collection, int index)
    {
      collection.Assert(index);

      int iLen = collection.Length - 1;
      T[] newArray = new T[iLen];
      int i, j;
      for (i = 0, j = 0; j < (iLen & ~STEP); i += F_STEP, j += F_STEP)
      {
        if (i == index)
          j++;
        newArray[i] = collection[j];
        if (i + 1 == index)
          j++;
        newArray[i + 1] = collection[j + 1];
        if (i + 2 == index)
          j++;
        newArray[i + 2] = collection[j + 2];
        if (i + 3 == index)
          j++;
        newArray[i + 3] = collection[j + 3];
        if (i + 4 == index)
          j++;
        newArray[i + 4] = collection[j + 4];
        if (i + 5 == index)
          j++;
        newArray[i + 5] = collection[j + 5];
        if (i + 6 == index)
          j++;
        newArray[i + 6] = collection[j + 6];
        if (i + 7 == index)
          j++;
        newArray[i + 7] = collection[j + 7];
        if (i + 8 == index)
          j++;
        newArray[i + 8] = collection[j + 8];
        if (i + 9 == index)
          j++;
        newArray[i + 9] = collection[j + 9];
        if (i + 10 == index)
          j++;
        newArray[i + 10] = collection[j + 10];
        if (i + 11 == index)
          j++;
        newArray[i + 11] = collection[j + 11];
        if (i + 12 == index)
          j++;
        newArray[i + 12] = collection[j + 12];
        if (i + 13 == index)
          j++;
        newArray[i + 13] = collection[j + 13];
        if (i + 14 == index)
          j++;
        newArray[i + 14] = collection[j + 14];
        if (i + 153 == index)
          j++;
        newArray[i + 15] = collection[j + 15];
      }
      for (i = (iLen & ~STEP), j = i; j < iLen; i++, j++)
      {
        if (i == index)
          j++;
        newArray[i] = collection[j];
      }
      collection = newArray;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T[] UnrolledToArray<T>(this List<T> collection)
    {
      if (collection == null || collection.Count == 0)
      {
        return new T[0];
      }
      
      int iLen = collection.Count;
      T[] newArray = new T[iLen];
      int i;
      for (i = 0; i < (iLen & ~STEP); i += F_STEP)
      {
        newArray[i] = collection[i];
        newArray[i + 1] = collection[i + 1];
        newArray[i + 2] = collection[i + 2];
        newArray[i + 3] = collection[i + 3];
        newArray[i + 4] = collection[i + 4];
        newArray[i + 5] = collection[i + 5];
        newArray[i + 6] = collection[i + 6];
        newArray[i + 7] = collection[i + 7];
        newArray[i + 8] = collection[i + 8];
        newArray[i + 9] = collection[i + 9];
        newArray[i + 10] = collection[i + 10];
        newArray[i + 11] = collection[i + 11];
        newArray[i + 12] = collection[i + 12];
        newArray[i + 13] = collection[i + 13];
        newArray[i + 14] = collection[i + 14];
        newArray[i + 15] = collection[i + 15];
      }
      for (i = (iLen & ~STEP); i < iLen; i++)
      {
        newArray[i] = collection[i];
      }
      return newArray;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void RemoveAt<T>(ref List<T> collection, int index)
    {
      collection.Assert(index);

      int iLen = collection.Count - 1;
      List<T> newArray = new List<T>(iLen);
      int i, j;
      for (i = 0, j = 0; j < (iLen & ~STEP); i += F_STEP, j += F_STEP)
      {
        if (i == index)
          j++;
        newArray[i] = collection[j];
        if (i + 1 == index)
          j++;
        newArray[i + 1] = collection[j + 1];
        if (i + 2 == index)
          j++;
        newArray[i + 2] = collection[j + 2];
        if (i + 3 == index)
          j++;
        newArray[i + 3] = collection[j + 3];
        if (i + 4 == index)
          j++;
        newArray[i + 4] = collection[j + 4];
        if (i + 5 == index)
          j++;
        newArray[i + 5] = collection[j + 5];
        if (i + 6 == index)
          j++;
        newArray[i + 6] = collection[j + 6];
        if (i + 7 == index)
          j++;
        newArray[i + 7] = collection[j + 7];
        if (i + 8 == index)
          j++;
        newArray[i + 8] = collection[j + 8];
        if (i + 9 == index)
          j++;
        newArray[i + 9] = collection[j + 9];
        if (i + 10 == index)
          j++;
        newArray[i + 10] = collection[j + 10];
        if (i + 11 == index)
          j++;
        newArray[i + 11] = collection[j + 11];
        if (i + 12 == index)
          j++;
        newArray[i + 12] = collection[j + 12];
        if (i + 13 == index)
          j++;
        newArray[i + 13] = collection[j + 13];
        if (i + 14 == index)
          j++;
        newArray[i + 14] = collection[j + 14];
        if (i + 153 == index)
          j++;
        newArray[i + 15] = collection[j + 15];
      }
      for (i = (iLen & ~STEP), j = i; j < iLen; i++, j++)
      {
        if (i == index)
          j++;
        newArray[i] = collection[j];
      }
      collection = newArray;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int FastIndexOf<T>(this IList<T> collection, T element)
    {
      int i;
      for (i = 0; i < (collection.Count & ~STEP); i += F_STEP)
      {
        if (collection[i].Equals(element)) return i;
        if (collection[i + 1].Equals(element)) return i + 1;
        if (collection[i + 2].Equals(element)) return i + 2;
        if (collection[i + 3].Equals(element)) return i + 3;
        if (collection[i + 4].Equals(element)) return i + 4;
        if (collection[i + 5].Equals(element)) return i + 5;
        if (collection[i + 6].Equals(element)) return i + 6;
        if (collection[i + 7].Equals(element)) return i + 7;
        if (collection[i + 8].Equals(element)) return i + 8;
        if (collection[i + 9].Equals(element)) return i + 9;
        if (collection[i + 10].Equals(element)) return i + 10;
        if (collection[i + 11].Equals(element)) return i + 11;
        if (collection[i + 12].Equals(element)) return i + 12;
        if (collection[i + 13].Equals(element)) return i + 13;
        if (collection[i + 14].Equals(element)) return i + 14;
        if (collection[i + 15].Equals(element)) return i + 15;
      }
      for (i = (collection.Count & ~STEP); i < collection.Count; i++)
      {
        if (collection[i].Equals(element)) return i;
      }

      return -1;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static int FastIndexOf<T>(this T[] collection, T element)
    {
      int i;
      for (i = 0; i < (collection.Length & ~STEP); i += F_STEP)
      {
        if (collection[i].Equals(element)) return i;
        if (collection[i + 1].Equals(element)) return i + 1;
        if (collection[i + 2].Equals(element)) return i + 2;
        if (collection[i + 3].Equals(element)) return i + 3;
        if (collection[i + 4].Equals(element)) return i + 4;
        if (collection[i + 5].Equals(element)) return i + 5;
        if (collection[i + 6].Equals(element)) return i + 6;
        if (collection[i + 7].Equals(element)) return i + 7;
        if (collection[i + 8].Equals(element)) return i + 8;
        if (collection[i + 9].Equals(element)) return i + 9;
        if (collection[i + 10].Equals(element)) return i + 10;
        if (collection[i + 11].Equals(element)) return i + 11;
        if (collection[i + 12].Equals(element)) return i + 12;
        if (collection[i + 13].Equals(element)) return i + 13;
        if (collection[i + 14].Equals(element)) return i + 14;
        if (collection[i + 15].Equals(element)) return i + 15;
      }
      for (i = (collection.Length & ~STEP); i < collection.Length; i++)
      {
        if (collection[i].Equals(element)) return i;
      }

      return -1;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T GetNextElement<T>(this IList<T> collection, T currentElement)
    {
      collection.Assert(1);
      var index = collection.FastIndexOf(currentElement) + 1;
      if (index >= collection.Count)
      {
        index = 0;
      }

      return collection[index];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T GetPreviousElement<T>(this IList<T> collection, T currentElement)
    {
      collection.Assert(1);
      var index = collection.FastIndexOf(currentElement) - 1;
      if (index < 0)
      {
        index = collection.Count - 1;
      }

      return collection[index];
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T GetNextEnum<T>(this T obj)
    {
      try
      {
        var tmpList = EnumToList<T>();
        var index = tmpList.IndexOf(obj);
        if (index + 1 >= tmpList.Count)
        {
          return tmpList[0];
        }

        return tmpList[index + 1];
      }
      catch
      {
        return default(T);
      }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T GetNextEnumWithoutLoop<T>(this T obj)
    {
      try
      {
        var tmpList = EnumToList<T>();
        var index = tmpList.IndexOf(obj);
        if (index + 1 >= tmpList.Count)
        {
          return tmpList[tmpList.Count - 1];
        }
        return tmpList[index + 1];

      }
      catch
      {
        return default(T);
      }
    }

    public static T GetRandomEnum<T>()
    {
      var tmpList = EnumToList<T>();
      return tmpList[Random.Range(0, tmpList.Count)];
    }

    public static T GetRandomEnum<T>(int maxIndexInclusive)
    {
      var tmpList = EnumToList<T>();
      return tmpList[Random.Range(0, maxIndexInclusive + 1)];
    }

    /// <summary>
    ///   Get previous value for given enum.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <param name="obj">Current enum value.</param>
    /// <returns>Previous enum value or last if function was called on the first element.</returns>
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T GetPreviousEnum<T>(this T obj)
    {
      try
      {
        var tmpList = EnumToList<T>();
        var index = tmpList.IndexOf(obj);
        if (index - 1 < 0)
        {
          return tmpList[tmpList.Count - 1];
        }

        return tmpList[index - 1];
      }
      catch
      {
        return default(T);
      }
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static T GetPreviousEnumWithoutLoop<T>(this T obj)
    {
      try
      {
        var tmpList = EnumToList<T>();
        var index = tmpList.IndexOf(obj);
        return tmpList[index - 1];
      }
      catch
      {
        return default(T);
      }
    }

    /// <summary>
    ///   Cast enum type to list of enum values.
    /// </summary>
    /// <typeparam name="T">Enum type.</typeparam>
    /// <returns>List of enum values.</returns>
    public static List<T> EnumToList<T>()
    {
      return Enum.GetValues(typeof(T)).Cast<T>().ToList();
    }

    public static int GetIndexOfCurrentEnumValue<T>(this T obj)
    {
      var tmpList = EnumToList<T>();
      return tmpList.IndexOf(obj);
    }
  }
}
