﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CollectionExtension;
using IndexedCollections;

namespace CollectionExtensionConsoleTests
{
    class Program
    {
        private static int length = 100000000;
      private static double tmp;
        static void Main(string[] args)
        {
            IndexedCollection<double> values = new IndexedCollection<double>(length);
            //double[] values = new double[length];
            for (int i = 0; i < length; i++)
            {
                values[i] = 5124561.5122d;
            }
            Stopwatch stopWatch = Stopwatch.StartNew();
            values.Each(d => tmp = d /= 3);
            stopWatch.Stop();
            Console.WriteLine(stopWatch.ElapsedMilliseconds);
            Console.ReadKey();
        }
    }
}
