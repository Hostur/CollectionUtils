﻿using System;
using System.Diagnostics;
using CollectionExtension;

namespace ConsoleApp1
{
    class Program
    {
        private static int length = 10000000;
        static void Main(string[] args)
        {
            double[] values = new double[length];
            for (int i = 0; i < length; i++)
            {
                values[i] = 5124561.5122d;
            }
            Stopwatch stopWatch = Stopwatch.StartNew();
            for (int i = 0; i < 100; i++)
            {
                values.Each(d => d /= 3);
            }
            
            stopWatch.Stop();
            Console.WriteLine(stopWatch.ElapsedMilliseconds);
            Console.ReadKey();
        }
    }
}
