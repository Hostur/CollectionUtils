﻿using System.Collections.Generic;
using CollectionExtension;

namespace IndexedCollections
{
  public class ConcurrencyQueue<T> where T : struct 
  {
    private List<T> _queue;

    public int Count
    {
      get
      {
        lock (_queue)
        {
          return _queue.Count;
        }
      }
    }

    public void Clear()
    {
      lock (_queue)
      {
        _queue.Clear();
      }
    }

    public ConcurrencyQueue(int capacity)
    {
      _queue = new List<T>(capacity);
    }

    public void Queue(T element)
    {
      lock (_queue)
      {
        _queue.Add(element);
      }
    }

    public bool TryDoDequeueAll(out T[] collection)
    {
      lock (_queue)
      {
        if (_queue.Count > 0)
        {
          collection = _queue.UnrolledToArray();
          _queue.Clear();
          return true;
        }    
      }

      collection = null;
      return false;
    }

    public bool TryDequeue(out T element)
    {
      lock (_queue)
      {
        if (_queue.Count > 0)
        {
          element = _queue[_queue.Count - 1];
          CollectionExtension.CollectionExtension.RemoveAt(ref _queue, _queue.Count);
          return true;
        }   
      }
      element = default(T);
      return false;
    }
  }
}
