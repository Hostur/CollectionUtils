﻿using System;
using CollectionExtension;

namespace IndexedCollections
{
  public class IndexedCollection<T> where T : struct
  {
    private T[] _collection;

    public T this[int index]
    {
        get => _collection[index];
        set => _collection[index] = value;
    }

    public void Resize(int capacity)
    {
        CollectionExtension.CollectionExtension.Resize(ref _collection, capacity);
    }

    public void Each(Action<T> action)
    {
      _collection.Each(action);
    }

    public IndexedCollection(int capacity)
    {
        _collection = new T[capacity];
    }
  }
}
