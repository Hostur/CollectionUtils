﻿using System;
using System.Runtime.CompilerServices;
using ProtoBuf;
using Unity.Entities;
using Unity.Mathematics;

namespace SerializableStructures
{
    [ProtoContract]
    public struct Chunk : IComponentData, IEquatable<Chunk>
    {
        [ProtoMember(1)]
        public int X { get; set; }

        [ProtoMember(2)]
        public int Y { get; set; }

        public Chunk(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Chunk(int2 value)
        {
            X = value.x;
            Y = value.y;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator ==(Chunk a, Chunk b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool operator !=(Chunk a, Chunk b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool Equals(Chunk other)
        {
            return X == other.X && Y == other.Y;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is Chunk chunk && Equals(chunk);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator int2(Chunk chunk)
        {
            return new int2(chunk.X, chunk.Y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator Chunk(int2 int2)
        {
            return new Chunk(int2);
        }
    }
}
