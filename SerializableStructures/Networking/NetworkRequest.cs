﻿using ProtoBuf;

namespace SerializableStructures.Networking
{
  [ProtoContract]
  public struct NetworkRequest
  {
    [ProtoMember(1)]
    public byte Id;

    [ProtoMember(2)]
    public byte[] Data;
  }
}
