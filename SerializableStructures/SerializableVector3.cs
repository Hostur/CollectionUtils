﻿using System;
using System.Runtime.CompilerServices;
using ProtoBuf;
using Unity.Mathematics;
using UnityEngine;

namespace SerializableStructures
{
    [Serializable]
    [ProtoContract]
    public struct SerializableVector3 
    {
        private const float SMALL_VALUE = 0.01F;

        [ProtoMember(1)]
        public float X;

        [ProtoMember(2)]
        public float Y;

        [ProtoMember(3)]
        public float Z;

        public SerializableVector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public SerializableVector3(Vector3 v3)
        {
            X = v3.x;
            Y = v3.y;
            Z = v3.z;
        }

        public SerializableVector3(float3 v3)
        {
            X = v3.x;
            Y = v3.y;
            Z = v3.z;
        }

        public SerializableVector3(Transform transform)
        {
            X = transform.position.x;
            Y = transform.position.y;
            Z = transform.position.z;
        }

        public bool IsZero => math.abs(X) < SMALL_VALUE &&
                              math.abs(Y) < SMALL_VALUE &&
                              math.abs(Z) < SMALL_VALUE;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float Distance(SerializableVector3 vector)
        {
            var difference = new SerializableVector3(X - vector.X, Y - vector.Y, Z - vector.Z);

            return math.sqrt(math.pow(difference.X, 2F) + math.pow(difference.Y, 2F) + math.pow(difference.Z, 2F));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector3 NormalizeDirection(Vector3 target)
        {
            return (target - (Vector3)this).normalized;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector3 operator -(SerializableVector3 vec1, SerializableVector3 vec2)
        {
            return new SerializableVector3(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector3 operator +(SerializableVector3 vec1, SerializableVector3 vec2)
        {
            return new SerializableVector3(vec1.X + vec2.X, vec1.Y + vec2.Y, vec1.Z + vec2.Z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector3 operator *(SerializableVector3 vec1, SerializableVector3 vec2)
        {
            return new SerializableVector3(vec1.X * vec2.X, vec1.Y * vec2.Y, vec1.Z * vec2.Z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector3 operator *(SerializableVector3 vec1, float value)
        {
            return new SerializableVector3(vec1.X * value, vec1.Y * value, vec1.Z * value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector3 operator /(SerializableVector3 vec1, float value)
        {
            if (value == 0)
            {
                throw new DivideByZeroException();
            }
            return new SerializableVector3(vec1.X / value, vec1.Y / value, vec1.Z / value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector3 Normalized(SerializableVector3 vector)
        {
            float magnitude = (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z);
            return new SerializableVector3(vector.X / magnitude, vector.Y / magnitude, vector.Z / magnitude);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator Vector3(SerializableVector3 vector)
        {
            return new Vector3(vector.X, vector.Y, vector.Z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator SerializableVector3(Vector3 vector)
        {
            return new SerializableVector3(vector.x, vector.y, vector.z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator Vector4(SerializableVector3 vector)
        {
            return new Vector4(vector.X, vector.Y, vector.Z, 0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator float4(SerializableVector3 vector)
        {
            return new float4(vector.X, vector.Y, vector.Z, 0);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator SerializableVector3(float4 vector)
        {
            return new SerializableVector3(vector.x, vector.y, vector.z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator SerializableVector3(Vector4 vector)
        {
            return new SerializableVector3(vector.x, vector.y, vector.z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator float3(SerializableVector3 vector)
        {
            return new float3(vector.X, vector.Y, vector.Z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator SerializableVector3(float3 vector)
        {
            return new SerializableVector3(vector.x, vector.y, vector.z);
        }
    }
}
