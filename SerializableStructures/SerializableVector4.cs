﻿using System;
using System.Runtime.CompilerServices;
using ProtoBuf;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Jobs;

namespace SerializableStructures
{
    [Serializable]
    [ProtoContract]
    public struct SerializableVector4
    {
        private const float SMALL_VALUE = 0.01F;

        [ProtoMember(1)]
        public float X;

        [ProtoMember(2)]
        public float Y;

        [ProtoMember(3)]
        public float Z;

        [ProtoMember(4)]
        public float W;

        public SerializableVector4(float x, float y, float z, float w)
        {
            X = x;
            Y = y;
            Z = z;
            W = w;
        }

        public SerializableVector4(Vector3 v3, float w)
        {
            X = v3.x;
            Y = v3.y;
            Z = v3.z;
            W = w;
        }

        public SerializableVector4(float3 v3, float w)
        {
            X = v3.x;
            Y = v3.y;
            Z = v3.z;
            W = w;
        }

        public SerializableVector4(ref TransformAccess transform)
        {
            X = transform.position.x;
            Y = transform.position.y;
            Z = transform.position.z;
            W = transform.localRotation.y;
        }

        public bool IsZero => math.abs(X) < SMALL_VALUE &&
                              math.abs(Y) < SMALL_VALUE &&
                              math.abs(Z) < SMALL_VALUE &&
                              math.abs(W) < SMALL_VALUE;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float Distance(SerializableVector4 vector)
        {
            var difference = new SerializableVector4(X - vector.X, Y - vector.Y, Z - vector.Z, 0);

            return math.sqrt(math.pow(difference.X, 2F) + math.pow(difference.Y, 2F) + math.pow(difference.Z, 2F));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector3 NormalizeDirection(Vector3 target)
        {
            return (target - this).normalized;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public Vector3 NormalizeDirection(SerializableVector4 target)
        {
            return ((Vector3)target - this).normalized;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector4 operator -(SerializableVector4 vec1, SerializableVector4 vec2)
        {
            return new SerializableVector4(vec1.X - vec2.X, vec1.Y - vec2.Y, vec1.Z - vec2.Z, vec1.W - vec2.W);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector4 operator +(SerializableVector4 vec1, SerializableVector4 vec2)
        {
            return new SerializableVector4(vec1.X + vec2.X, vec1.Y + vec2.Y, vec1.Z + vec2.Z, vec1.W + vec2.W);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector4 operator *(SerializableVector4 vec1, SerializableVector4 vec2)
        {
            return new SerializableVector4(vec1.X * vec2.X, vec1.Y * vec2.Y, vec1.Z * vec2.Z, vec1.W * vec2.W);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector4 operator *(SerializableVector4 vec1, float value)
        {
            return new SerializableVector4(vec1.X * value, vec1.Y * value, vec1.Z * value, vec1.W * value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector4 operator /(SerializableVector4 vec1, float value)
        {
            if (value == 0)
            {
                throw new DivideByZeroException();
            }
            return new SerializableVector4(vec1.X / value, vec1.Y / value, vec1.Z / value, vec1.W / value);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static SerializableVector4 Normalized(SerializableVector4 vector)
        {
            float magnitude = (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z);
            return new SerializableVector4(vector.X / magnitude, vector.Y / magnitude, vector.Z / magnitude, vector.W);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator Vector3(SerializableVector4 vector)
        {
            return new Vector3(vector.X, vector.Y, vector.Z);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator Vector4(SerializableVector4 vector)
        {
            return new Vector4(vector.X, vector.Y, vector.Z, vector.W);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator float4(SerializableVector4 vector)
        {
            return new float4(vector.X, vector.Y, vector.Z, vector.W);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator SerializableVector4(float4 vector)
        {
            return new SerializableVector4(vector.x, vector.y, vector.z, vector.w);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator SerializableVector4(Vector4 vector)
        {
            return new SerializableVector4(vector.x, vector.y, vector.z, vector.w);
        }
    }
}
