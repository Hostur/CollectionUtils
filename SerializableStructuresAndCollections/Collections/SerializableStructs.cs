﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SerializableStructuresAndCollections.Collections
{
    public class SerializableStructs<T> where T : struct
    {
        private T[] _collection;
        public T this[int length] => _collection[length];

        public ref T GetRef(int index)
        {
            return ref _collection[index];
        }

        public SerializableStructs(int length)
        {
            _collection = new T[length];
        }
    }
}
